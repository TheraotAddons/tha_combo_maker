extends "input_base.gd"


const InputBase = preload("input_base.gd")


var inputs:Array setget _no_set
var operator:String = "?"


func _init():
	inputs = []


func _process_input(input_event:InputEvent) -> ProcessResult:
	var result := ProcessResult.new()
	for input in inputs:
		var input_base := input as InputBase
		var old_status:int = input_base.get_status()
		if old_status == SUCCEDED:
			continue
			
		if old_status == FAILED:
			break

		var status := old_status
		if not result.input_consumed and input_base.process_input(input_event):
			status = input_base.get_status()
			result.input_consumed = true

			if status != old_status:
				_status_changed(input_base)
				if status == FAILED:
					break

	result.new_status = _status_update()
	return result


func _reset() -> void:
	for input in inputs:
		(input as InputBase).reset()


func _status_changed(input:InputBase) -> void:
	pass


func _status_update() -> int:
	var final_status := SUCCEDED
	for input in inputs:
		var input_base := input as InputBase
		var status:int = input_base.get_status()
		if status == FAILED:
			return FAILED

		final_status = min(final_status, status)

	return final_status


func _no_set(value) -> void:
	pass


func _to_string() -> String:
	var first:bool = true
	var result:String = ""
	for input in inputs:
		if first:
			first = false
		else:
			result += " " + operator + " "

		result += str(input)

	if inputs.size() < 2:
		result = result + " " + operator

	result = "(" + result + ")"
	return result
