extends "input_composite.gd"


func _init() -> void:
	operator = "+"


func _reset() -> void:
	for input in inputs:
		var input_base := input as InputBase
		input_base.reset()
		input_base.allow_start = true
		input_base.allow_success = false


func _status_changed(input:InputBase) -> void:
	var status := input.get_status()
	if status != STARTED:
		return

	var all_started := true
	for input in inputs:
		var input_base := input as InputBase
		if input_base.get_status() != STARTED:
			all_started = false
			break

	for input in inputs:
		var input_base := input as InputBase
		input_base.allow_success = all_started
