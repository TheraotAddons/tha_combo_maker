extends Object


var _source:String
var _position:int


func _init(source:String):
	_source = source
	_position = 0


func end_of_string() -> bool:
	return _position == _source.length()


func consume_string(text:String) -> bool:
	var length := text.length()
	if _source.substr(_position, length) == text:
		_position += length
		return true

	return false


func consume_until(texts:Array, greedy:bool) -> String:
	var best_position := _source.length()
	for text in texts:
		var position := _source.findn(str(text), _position)
		if position == -1:
			continue

		if position < best_position:
			best_position = position
	
	var result := ""
	if best_position == _source.length():
		if greedy:
			result = _source.substr(_position)
			_position = _source.length()
			return result
		else:
			return ""

	result = _source.substr(_position, best_position - _position)
	_position = best_position
	return result


func consume_while(texts:Array) -> String:
	var initial_position := _position
	while (true):
		var found_anything := false
		for text in texts:
			var s := str(text)
			var length := s.length()
			if _source.substr(_position, length) == s:
				_position += length
				found_anything = true

		if not found_anything:
			break

	return _source.substr(initial_position, _position - initial_position)
