extends "input_composite.gd"


func _init() -> void:
	operator = ">"


func _reset() -> void:
	var first := true
	for input in inputs:
		var input_base := input as InputBase
		input_base.reset()
		input_base.allow_start = first
		input_base.allow_success = false
		first = false


func _status_changed(input:InputBase) -> void:
	var status := input.get_status()
	if status != STARTED:
		return

	var index := inputs.find(input)
	var is_first := index == 0
	var is_last := index == inputs.size() - 1
	if is_last:
		input.allow_success = true
	else:
		var input_base :=  inputs[index + 1] as InputBase
		input_base.allow_start = true
	
	if not is_first:
		var input_base :=  inputs[index - 1] as InputBase
		input_base.allow_success = true
