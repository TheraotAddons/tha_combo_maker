extends "input_simultaneous.gd"


func _init() -> void:
	operator = "&"


func _status_update() -> int:
	var final_status := SUCCEDED
	for input in inputs:
		var input_base := input as InputBase
		var status:int = input_base.get_status()
		if status == FAILED:
			return FAILED

		final_status = status

	return final_status
