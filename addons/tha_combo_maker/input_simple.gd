extends "input_base.gd"


var action:String


func _process_input(input_event:InputEvent) -> ProcessResult:
	var status := get_status()
	var result := ProcessResult.new()
	result.new_status = status
	var looking_for_presssed:bool
	var allow:bool
	var next_status:int
	match status:
		UNSTARTED:
			looking_for_presssed = true
			allow = allow_start
			next_status = STARTED
		STARTED:
			looking_for_presssed = false
			allow = true
			next_status = SUCCEDED
		_:
			return result

	if input_event.is_action(action) and input_event.is_pressed() == looking_for_presssed:
		result.new_status = next_status
		result.input_consumed = true
		return result

	if Input.is_action_pressed(action) == looking_for_presssed and allow:
		result.new_status = next_status

	return result


func _to_string() -> String:
	return "\"" + action + "\""
