extends "input_composite.gd"


func _init() -> void:
	operator = "|"


func _reset() -> void:
	for input in inputs:
		var input_base := input as InputBase
		input_base.reset()
		input_base.allow_start = true
		input_base.allow_success = true


func _status_update() -> int:
	var final_status := UNSTARTED
	for input in inputs:
		var input_base := input as InputBase
		var status:int = input_base.get_status()
		if status == FAILED:
			return FAILED

		final_status = max(final_status, status)

	return final_status
