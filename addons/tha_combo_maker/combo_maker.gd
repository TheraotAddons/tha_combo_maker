extends Node


const InputBase = preload("input_base.gd")
const InputSimple = preload("input_simple.gd")
const InputAny = preload("input_any.gd")
const InputComposite = preload("input_composite.gd")
const InputSequential = preload("input_sequential.gd")
const InputTraveling = preload("input_traveling.gd")
const InputConcurrent = preload("input_concurrent.gd")
const InputSimultaneous = preload("input_simultaneous.gd")
const InputDisjunction = preload("input_disjunction.gd")
const operator_map := {
	",": InputSequential,
	">": InputTraveling,
	"&": InputConcurrent,
	"+": InputSimultaneous,
	"|": InputDisjunction,
}
const Parser = preload("parser.gd")


signal pressed
signal released


export var inputs:String setget set_inputs
export var reset_time:int = 1000
export var max_time_to_press:int = 0
export var min_time_to_hold:int = 0
export var negate_output:bool
export var pressed:bool setget set_pressed
export var emulated_action:String


var _parsed_inputs:InputBase
var _last_time:int
var _last_event:InputEvent


func _ready() -> void:
	set_physics_process(false)
	set_pressed(negate_output)


func _input(event: InputEvent) -> void:
	if (
		event is InputEventScreenDrag
		or event is InputEventScreenTouch
		or event is InputEventMouse
	):
		return
	
	if _parsed_inputs == null:
		return
		
	_last_event = event
	var forbid_reset := false
	var found_status := _parsed_inputs.get_status()
	var now := OS.get_ticks_msec()
	if (
		(found_status == InputBase.UNSTARTED and now - _last_time > reset_time)
		or found_status == InputBase.FAILED
		or found_status == InputBase.SUCCEDED
	):
		_last_time = now
		_parsed_inputs.reset()
		forbid_reset = true

	while true:
		if _parsed_inputs.process_input(event):
			_last_time = now

		var status:int = _parsed_inputs.get_status()
		if max_time_to_press > 0 and status == InputBase.STARTED and _parsed_inputs.last_status_duration > max_time_to_press:
			status = InputBase.FAILED
			forbid_reset = true

		if min_time_to_hold > 0 and status == InputBase.STARTED and (now - _parsed_inputs.status_change_time) < min_time_to_hold:
			status = InputBase.UNSTARTED
			set_physics_process(true)
		else:
			set_physics_process(false)

		set_pressed((status == InputBase.STARTED) != negate_output)
		if status == InputBase.FAILED and not forbid_reset:
			_parsed_inputs.reset()
			forbid_reset = true
			continue
		else:
			break


func _physics_process(delta: float) -> void:
	if _last_event == null:
		set_physics_process(false)
		return

	_input(_last_event)


func set_inputs(value:String) -> void:
	if inputs == value:
		return

	inputs = value
	_parsed_inputs = _parse_input(value)
	_parsed_inputs.reset()
	_last_time = OS.get_ticks_msec()


func set_pressed(value:bool) -> void:
	if pressed == value:
		return

	pressed = value
	if InputMap.has_action(emulated_action):
		var e := InputEventAction.new()
		e.action = emulated_action
		e.pressed = pressed
		e.strength = 1.0 if pressed else 0.0
		Input.parse_input_event(e)

	if pressed:
		emit_signal("pressed")
	else:
		emit_signal("released")


static func _parse_input(value:String) -> InputBase:
	var result:InputBase = null
	var parser := Parser.new(value)
	var stack := []
	while not parser.end_of_string():
		var input:InputBase = _consume_input(parser, result)
		if input != null:
			if result is InputComposite:
				if input is InputComposite:
					if not result.parenthesized and _priority(result) > _priority(input):
						input.inputs.append(result.inputs.pop_back())
						result.inputs.push_back(input)
						stack.push_back(result)
						result = input
					else:
						input.inputs.push_back(result)
						result = input
				else:
					result.inputs.push_back(input)
			elif result == null:
				result = input
			else:
				if input is InputComposite:
					input.inputs.push_back(result)
					result = input
				else:
					push_error("Missing connector")

	if stack.size() > 0:
		return stack[0]
	else:
		return result


static func _consume_input(parser:Parser, result:InputBase) -> InputBase:
	parser.consume_while([" ", "\t", "\n", "\r"])
	if parser.consume_string("\""):
		var action := parser.consume_until(["\""], true)
		parser.consume_string("\"")
		var input := InputSimple.new()
		input.action = action
		return input

	for operator in operator_map.keys():
		if parser.consume_string(operator):
			if result is InputComposite and (result as InputComposite).operator == operator:
				return null
			else:
				return operator_map[operator].new()

	if parser.consume_string("*"):
		return InputAny.new()

	if parser.consume_string("("):
		var parenthesis_count := 1
		var rest := ""
		while true:
			rest += parser.consume_until(["\"", ")", "("], true)
			if parser.consume_string("\""):
				rest += "\"" + parser.consume_until(["\""], true) + "\""
				parser.consume_string("\"")
				continue
			elif parser.consume_string("("):
				parenthesis_count += 1
			elif parser.consume_string(")"):
				parenthesis_count -= 1
				if parenthesis_count == 0:
					break

		parser.consume_while([" ", "\t", "\n", "\r"])
		var input := _parse_input(rest)
		input.parenthesized = true
		return input

	var action := parser.consume_until([" ", "\t", "\n", "\r", ",", ">", "+"], true)
	var input := InputSimple.new()
	input.action = action
	return input


static func _priority(input:InputComposite) -> int:
	var values := operator_map.values()
	for index in values.size():
		if input is values[index]:
			return values.size() - index

	return 0
