extends Object


var input_consumed:bool
var new_status:int


func _to_string() -> String:
	return str(input_consumed) + " (" + str(new_status) + ")"
