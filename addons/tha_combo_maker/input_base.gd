extends Object


const ProcessResult := preload("process_result.gd")


enum {UNSTARTED, STARTED, SUCCEDED, FAILED}


var parenthesized:bool = false
var allow_start:bool = true
var allow_success:bool = true
var status_change_time:int setget _no_set
var last_status_duration:int setget _no_set


var _status:int setget set_status, get_status


func _init() -> void:
	last_status_duration = 0
	status_change_time = OS.get_ticks_msec()
	reset()


func process_input(input_event:InputEvent) -> bool:
	var old_status := get_status()
	if old_status == SUCCEDED or old_status == FAILED:
		return false

	var process_result := _process_input(input_event)
	var new_status := process_result.new_status
	if new_status == STARTED and not allow_start:
		new_status = FAILED
	elif new_status == SUCCEDED and not allow_success:
		new_status = FAILED

	set_status(new_status)
	return process_result.input_consumed


func reset() -> void:
	_reset()
	allow_start = true
	allow_success = true
	set_status(UNSTARTED)


func set_status(value:int) -> void:
	if _status == value:
		return

	_status = value
	var now := OS.get_ticks_msec()
	last_status_duration = now - status_change_time
	status_change_time = now


func get_status() -> int:
	return _status


func _reset() -> void:
	pass


func _process_input(input_event:InputEvent) -> ProcessResult:
	var result := ProcessResult.new()
	result.new_status = get_status()
	return result


func _no_set(value) -> void:
	pass
